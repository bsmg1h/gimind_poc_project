﻿# coding:utf-8


import copy
import pandas as pd
import numpy as np
from math import log
from tools.IO import dingzeng_loader
from tools.IO import dingzeng_saver

import io
from io import open
import os
import re
import sys
import json
import argparse
from label_loader import LabelLoader

reload(sys)
sys.setdefaultencoding('utf-8')


class DingzengHandler:

    def __init__(self, dingzengDataFolder):

        self.dingzengDataFolder = dingzengDataFolder
        self.dingzengFileList = os.listdir(self.dingzengDataFolder)

        self.dingzengData = {
            int(re.sub('.html.json', '', dingzengFile)):
                json.load(open(self.dingzengDataFolder + '/' + dingzengFile, 'r', encoding='utf-8'))
            for dingzengFile in self.dingzengFileList
        }

        self.dingzengIDList = [int(re.sub('.html.json', '', dingzengFile)) for dingzengFile in self.dingzengFileList]
        self.dingzengIDList.sort()

        self.dingzengDataOnlyDict = {}
        for k, dingzengItem in self.dingzengData.items():
            self.dingzengDataOnlyDict[k] = []
            for textItem in dingzengItem:
                if isinstance(textItem, dict):
                    self.dingzengDataOnlyDict[k].append(textItem)

        self.dingzengImportantTableNames = {}
        with open('dict/dingzeng_important_table_names.csv', 'r', encoding='utf-8') as fp:
            for line in fp.readlines():
                line = re.sub(u'\ufeff', '', string=line)
                line = re.split(string=line, pattern=',')
                self.dingzengImportantTableNames[line[0]] = line[1]

        pass

    def dingzeng_table_struc(self, dingzengID):
        dingzengDictItem = self.dingzengDataOnlyDict[dingzengID]
        nDict = len(dingzengDictItem)
        # print('公告',dingzengID,'共有',nDict,'个表格,它们的表头分别是')
        totalColumnNames = []
        for i in range(nDict):
            thisDict = dingzengDictItem[i]
            thisColumnNames = []
            for k, v in thisDict.items():
                if v:
                    thisColumnNames.append(v[0])
            # print(i, '\t\t', thisColumnNames)
            totalColumnNames.append(thisColumnNames)
        return totalColumnNames


    def getImportantDictTest(self, dingzengID):
        dictList = self.dingzengDataOnlyDict[dingzengID]
        thisDictColumns = self.dingzeng_table_struc(dingzengID)

        def getDictColumnsScore(dictColumn):
            if not dictColumn:
                return 0
            score = 0
            for name in dictColumn:
                if name in self.dingzengImportantTableNames:
                    score += 0.3 + (log(int(self.dingzengImportantTableNames[name])))
            return score / len(dictColumn)

        dictScore = [getDictColumnsScore(dictColumn) for dictColumn in thisDictColumns]
        if not dictScore:
            return None, 0, 0
        dictIndex = dictScore.index(max(dictScore))
        return dictList[dictIndex], dictIndex, max(dictScore)


    def is_number(self, s):

        if not s:
            return False

        try:
            float(s)
            return True
        except ValueError:
            pass

        try:
            import unicodedata
            unicodedata.numeric(s)
            return True
        except (TypeError, ValueError):
            pass

        return False

    def informationExtractor(self, dingzengID):
        importantDict, _, score = copy.deepcopy(self.getImportantDictTest(dingzengID))

        if score < 1:
            print(dingzengID)
            print('公告内没有关键表格，需要从文本中寻找信息')
            return False

        if "-1" in importantDict:
            del importantDict['-1']

        keyColumns = {
            '增发对象'.decode('utf8'): None,
            '增发数量'.decode('utf8'): None,
            '增发单价'.decode('utf8'): None,
            '增发金额'.decode('utf8'): None,
            '锁定期'.decode('utf8'): None,
            '认购方式'.decode('utf8'): None,
        }

        for k, v in importantDict.items():
            importantDict[k] = [re.sub(u','.decode('utf8'), '', item) for item in v]

        # 增发对象
        for i in range(len(importantDict)):
            if re.search(u'对象|名称|人|称|机构'.decode('utf8'), importantDict[str(i)][0]):
                keyColumns['增发对象'.decode('utf8')] = str(i)
                break

        if not keyColumns['增发对象'.decode('utf8')]:
            print(dingzengID)
            print('没有从关键表格中找到有意义的实体，需要从文本中寻找信息')
            return False

        # 增发数量
        for i in range(len(importantDict)):
            if re.search(u'数'.decode('utf8'), importantDict[str(i)][0]):
                keyColumns['增发数量'.decode('utf8')] = str(i)

                # 补齐
                while len(importantDict[str(i)]) < len(importantDict[keyColumns['增发对象'.decode('utf8')]]):
                    importantDict[str(i)].append(importantDict[str(i)][-1])

                # 转化成数字
                for j in range(1, len(importantDict[str(i)])):
                    if self.is_number(importantDict[str(i)][j]):
                        importantDict[str(i)][j] = float(importantDict[str(i)][j])
                        if re.search(u'万'.decode('utf8'), importantDict[str(i)][0]):
                            importantDict[str(i)][j] *= 10000
                        if re.search(u'亿'.decode('utf8'), importantDict[str(i)][0]):
                            importantDict[str(i)][j] *= 100000000
                break

        # 增发单价
        for i in range(len(importantDict)):
            if re.search(u'价'.decode('utf8'), importantDict[str(i)][0]):
                keyColumns['增发单价'.decode('utf8')] = str(i)

                # 补齐
                while len(importantDict[str(i)]) < len(importantDict[keyColumns['增发对象'.decode('utf8')]]):
                    importantDict[str(i)].append(importantDict[str(i)][-1])

                # 转化成数字
                for j in range(1, len(importantDict[str(i)])):
                    if self.is_number(importantDict[str(i)][j]):
                        importantDict[str(i)][j] = float(importantDict[str(i)][j])
                        if re.search(u'万'.decode('utf8'), importantDict[str(i)][0]):
                            importantDict[str(i)][j] *= 10000
                        if re.search(u'亿'.decode('utf8'), importantDict[str(i)][0]):
                            importantDict[str(i)][j] *= 100000000
                break

        # 增发金额
        for i in range(len(importantDict)):
            if re.search(u'额'.decode('utf8'), importantDict[str(i)][0]):
                keyColumns['增发金额'.decode('utf8')] = str(i)

                # 补齐
                while len(importantDict[str(i)]) < len(importantDict[keyColumns['增发对象'.decode('utf8')]]):
                    importantDict[str(i)].append(importantDict[str(i)][-1])

                # 转化成数字
                for j in range(1, len(importantDict[str(i)])):
                    if self.is_number(importantDict[str(i)][j]):
                        importantDict[str(i)][j] = float(importantDict[str(i)][j])
                        if re.search(u'万'.decode('utf8'), importantDict[str(i)][0]):
                            importantDict[str(i)][j] *= 10000
                        if re.search(u'亿'.decode('utf8'), importantDict[str(i)][0]):
                            importantDict[str(i)][j] *= 100000000
                break

        # 锁定期
        for i in range(len(importantDict)):
            if re.search(u'限售期|锁定期'.decode('utf8'), importantDict[str(i)][0]):
                keyColumns['锁定期'.decode('utf8')] = str(i)

                # 补齐
                while len(importantDict[str(i)]) < len(importantDict[keyColumns['增发对象'.decode('utf8')]]):
                    importantDict[str(i)].append(importantDict[str(i)][-1])
                break

        # 认购方式
        for i in range(len(importantDict)):
            if re.search(u'方式'.decode('utf8'), importantDict[str(i)][0]):
                keyColumns['认购方式'.decode('utf8')] = str(i)

                # 补齐
                while len(importantDict[str(i)]) < len(importantDict[keyColumns['增发对象'.decode('utf8')]]):
                    importantDict[str(i)].append(importantDict[str(i)][-1])
                break

        ret = {}
        for i in range(1, len(importantDict[keyColumns['增发对象'.decode('utf8')]])):
            if re.search(u'合计|总计|对象|名称'.decode('utf8'), importantDict[keyColumns['增发对象'.decode('utf8')]][i]):
                continue
            thisEntity = importantDict[keyColumns['增发对象'.decode('utf8')]][i]
            if thisEntity in ret:
                continue
            ret[thisEntity] = {
                '增发数量'.decode('utf8'): int(),
                '增发单价'.decode('utf8'): float(),
                '增发金额'.decode('utf8'): float(),
                '锁定期'.decode('utf8'): '',
                '认购方式'.decode('utf8'): ''
            }

            for key in ret[thisEntity]:
                if keyColumns[key]:
                    if i > len(importantDict[keyColumns[key]]) - 1:
                        ret[thisEntity][key] = importantDict[keyColumns[key]][1]
                    ret[thisEntity][key] = importantDict[keyColumns[key]][i]

            # 最后格式调整
        for k, v in ret.items():

            if not v['锁定期'.decode('utf8')]:
                ret[k]['锁定期'.decode('utf8')] = 12
                continue
            elif self.is_number(v['锁定期'.decode('utf8')]):
                ret[k]['锁定期'.decode('utf8')] = int(v['锁定期'.decode('utf8')])
            elif re.findall(u'[0-9][0-9]个月'.decode('utf8'), v['锁定期'.decode('utf8')]):
                ret[k]['锁定期'.decode('utf8')] = re.findall(u'[0-9][0-9]个月'.decode('utf8'), v['锁定期'.decode('utf8')])[0]
                ret[k]['锁定期'.decode('utf8')] = re.sub(u'个月'.decode('utf8'), '', ret[k]['锁定期'.decode('utf8')])
                ret[k]['锁定期'.decode('utf8')] = int(ret[k]['锁定期'.decode('utf8')])
            else:
                ret[k]['锁定期'.decode('utf8')] = 12

        for k, v in ret.items():
            if not v['认购方式'.decode('utf8')]:
                v['认购方式'.decode('utf8')] = '现金'.decode('utf8')

        for k, v in ret.items():
            if type(v['增发数量'.decode('utf8')]) in (str, unicode):
                ret[k]['增发数量'.decode('utf8')] = re.sub(u'[\u4e00-\u9fff|-]'.decode('utf8'), '', v['增发数量'.decode('utf8')].decode('utf8'))
                if self.is_number(ret[k]['增发数量'.decode('utf8')]):
                    ret[k]['增发数量'.decode('utf8')] = int(float(ret[k]['增发数量'.decode('utf8')]))
                else:
                    ret[k]['增发数量'.decode('utf8')] = int()
            if ret[k]['增发数量'.decode('utf8')] <= 15001:
                ret[k]['增发数量'.decode('utf8')] *= 10000

            if type(v['增发单价'.decode('utf8')]) in (str, unicode):
                ret[k]['增发单价'.decode('utf8')] = re.sub(u'[\u4e00-\u9fff]'.decode('utf8'), '', v['增发单价'.decode('utf8')])

            if type(v['增发金额'.decode('utf8')]) in (str, unicode):
                ret[k]['增发金额'.decode('utf8')] = re.sub(u'[\u4e00-\u9fff]'.decode('utf8'), '', v['增发金额'.decode('utf8')])
                if self.is_number(ret[k]['增发金额'.decode('utf8')]):
                    ret[k]['增发金额'.decode('utf8')] = float(ret[k]['增发金额'.decode('utf8')])
                else:
                    ret[k]['增发金额'.decode('utf8')] = int()

        return keyColumns, importantDict, ret

    def getResult(self):

        a = 0
        result = {
            '公告id'.decode('utf8'): [],
            '增发对象'.decode('utf8'): [],
            '增发数量'.decode('utf8'): [],
            '增发单价'.decode('utf8'): [],
            '增发金额'.decode('utf8'): [],
            '锁定期'.decode('utf8'): [],
            '认购方式'.decode('utf8'): [],
        }

        for dingzengID in self.dingzengIDList:

            if not self.informationExtractor(dingzengID):
                a += 1
                continue

            tempResult = self.informationExtractor(dingzengID)[2]
            for k, v in tempResult.items():

                if not re.search(u'[\u4e00-\u9fff]|[a-z][A-Z]', k):
                    continue

                result['公告id'.decode('utf8')].append(dingzengID)

                name = re.sub(u'（'.decode('utf8'), '('.decode('utf8'), k)
                name = re.sub(u'）'.decode('utf8'), ')'.decode('utf8'), name)

                result['增发对象'.decode('utf8')].append(name)
                result['增发数量'.decode('utf8')].append(v['增发数量'.decode('utf8')])
                result['增发单价'.decode('utf8')].append(v['增发单价'.decode('utf8')])
                result['增发金额'.decode('utf8')].append(v['增发金额'.decode('utf8')])
                result['锁定期'.decode('utf8')].append(v['锁定期'.decode('utf8')])
                result['认购方式'.decode('utf8')].append(v['认购方式'.decode('utf8')])

        result['公告id'.decode('utf8')] = np.array(result['公告id'.decode('utf8')], dtype='int')
        result['增发对象'.decode('utf8')] = np.array(result['增发对象'.decode('utf8')], dtype='unicode')
        result['增发数量'.decode('utf8')] = np.array(result['增发数量'.decode('utf8')], dtype='float')
        result['增发数量'.decode('utf8')] = np.array(result['增发数量'.decode('utf8')], dtype='int')

        df = pd.DataFrame(result)

        df['增发数量'.decode('utf8')][df['增发数量'.decode('utf8')] == 0] = ''
        df['增发金额'.decode('utf8')][df['增发金额'.decode('utf8')] == 0] = ''

        del df['增发单价'.decode('utf8')]

        df = df[['公告id'.decode('utf8'), '增发对象'.decode('utf8'), '增发数量'.decode('utf8'),
                 '增发金额'.decode('utf8'), '锁定期'.decode('utf8'), '认购方式'.decode('utf8')]]

        print(a)

        return df


if __name__ == '__main__':

    dingzengDataFolder = 'testdatab/dingzeng/json'

    a = DingzengHandler(dingzengDataFolder)
    result = a.getResult()

    dingzeng_saver(obj=result, dir='dingzeng_test_on_testb_output_python2.csv', encoding='utf-8', header=False)






