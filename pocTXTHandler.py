import os
import re
import shutil
# from bs4 import BeautifulSoup
import json


CN_NUM = {
    '〇': 0, '一': 1, '二': 2, '三': 3, '四': 4, '五': 5, '六': 6, '七': 7, '八': 8, '九': 9, '零': 0,
    '壹': 1, '贰': 2, '叁': 3, '肆': 4, '伍': 5, '陆': 6, '柒': 7, '捌': 8, '玖': 9, '貮': 2, '两': 2,
}

CN_UNIT = {
    '十': 10,
    '拾': 10,
    '百': 100,
    '佰': 100,
    '千': 1000,
    '仟': 1000,
    '万': 10000,
    '萬': 10000,
    '亿': 100000000,
    '億': 100000000,
    '兆': 1000000000000,
}

def chinese_to_arabic(cn:str):
    unit = 0   # current
    ldig = []  # digest
    for cndig in reversed(cn):
        if cndig in CN_UNIT:
            unit = CN_UNIT.get(cndig)
            if unit == 10000 or unit == 100000000:
                ldig.append(unit)
                unit = 1
        else:
            dig = CN_NUM.get(cndig)
            if unit:
                dig *= unit
                unit = 0
            ldig.append(dig)
    if unit == 10:
        ldig.append(10)
    val, tmp = 0, 0
    for x in reversed(ldig):
        if x == 10000 or x == 100000000:
            val += tmp * x
            tmp = 0
        else:
            tmp += x
    val += tmp
    if val == 0:
        return ''
    return val

def is_number(s):
    if not s:
        return False

    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False

def locate(pattern, textlist):
    pos = 0
    for text in textlist:
        if re.search(pattern, text):
            return pos
        pos += 1
    pos = 0
    return pos



def locatesearchlist(patternlist, textlist):
    for pos in range(len(textlist) - len(patternlist)):
        tl = textlist[pos:(pos+len(patternlist))]
        for i in range(len(patternlist)):

            if patternlist[i] == '':
                if tl[i] == '':
                    pass
                else:
                    break
            else:
                if re.search(patternlist[i], tl[i]):
                    pass
                else:
                    break

            if i == len(patternlist) - 1:
                return pos, pos+len(patternlist), tl
    return -1, -1, []

def locatematchlist(patternlist, textlist):
    for pos in range(len(textlist) - len(patternlist)):
        tl = textlist[pos:(pos+len(patternlist))]
        for i in range(len(patternlist)):

            if patternlist[i] == '':
                if tl[i] == '':
                    pass
                else:
                    break
            else:
                if re.match(patternlist[i], tl[i]):
                    pass
                else:
                    break

            if i == len(patternlist) - 1:
                return pos, pos+len(patternlist), tl
    return -1, -1, []

def locateequallist(patternlist, textlist):
    for pos in range(len(textlist) - len(patternlist)):
        tl = textlist[pos:(pos+len(patternlist))]
        for i in range(len(patternlist)):

            if patternlist[i] == tl[i]:
                pass
            else:
                break

            if i == len(patternlist) - 1:
                return pos, pos+len(patternlist), tl
    return -1, -1, []

class poc:

    def __init__(self, dir, file):

        self.textlist = []
        with open(dir + '/' + file, 'r', encoding='utf-8') as fp:
            for line in fp.readlines():
                self.textlist.append(re.sub('\n', '', line))

        print(file, len(self.textlist))

        self.data = {
            '文件名': re.sub('.html', '', file),
            '发行人': file.split('20')[0],
            '主承销商': self.lead_underwriter(),
            '联席主承销商': self.joint_lead_underwriter(),
            '发行金额（元）': self.issue_amount(),
            '发行期限': self.issue_period(),
            '面值（元）': self.face_value(),
            '发行日': self.issue_date(),
            '起息日': self.value_date(),
        }

        # print(self.data['起息日'])

    def lead_underwriter(self):

        def normalize(text):
            ret = re.findall('.*公司|.*银行', text)[0]
            ret = ret.split('\n')[-1]
            ret = ret.split('二')[0]
            ret = ret.split('人')[-1]
            return ret

        mainpos = locate('目', self.textlist)
        if mainpos == 0:
            mainpos = locate('第一章', self.textlist)
        if mainpos == 0:
            mainpos = len(self.textlist)
        ret = ''
        for n, text in enumerate(self.textlist[:mainpos]):
            if re.search('主承销商.*公司|主承销商.*银行', text):
                ret = re.findall('主承销商.*?公司|主承销商.*?银行', text)[0]
                ret = ret.split('：')[-1]
                ret = normalize(ret)
                return ret

        for n, text in enumerate(self.textlist[:mainpos]):
            if re.search('主承销商', text):
                ret = re.findall('.*?公司|.*?银行', self.textlist[n+1])
                if ret:
                    return normalize(ret[0])
                else:
                    ret = re.findall('.*?公司|.*?银行', self.textlist[n + 2])
                    if ret:
                        return normalize(ret[0])

        if not ret:
            ret = ''

        return ret


    def joint_lead_underwriter(self):
        def normalize(text):
            ret = re.findall('.*公司|.*银行', text)[0]
            ret = ret.split('\n')[-1]
            ret = ret.split('二')[0]
            ret = ret.split('人')[-1]
            return ret

        mainpos = locate('目', self.textlist)
        if mainpos == 0:
            mainpos = locate('第一章', self.textlist)
        if mainpos == 0:
            mainpos = len(self.textlist)
        ret = ''
        for n, text in enumerate(self.textlist[:mainpos]):
            if re.search('联席主承销商.*公司|联席主承销商.*银行', text):
                ret = re.findall('联席主承销商.*?公司|联席主承销商.*?银行', text)[0]
                ret = ret.split('：')[-1]
                ret = normalize(ret)
                return ret

        for n, text in enumerate(self.textlist[:mainpos]):
            if re.search('联席主承销商', text):
                ret = re.findall('.*?公司|.*?银行', self.textlist[n + 1])
                if ret:
                    return normalize(ret[0])
                else:
                    ret = re.findall('.*?公司|.*?银行', self.textlist[n + 2])
                    if ret:
                        return normalize(ret[0])

        if not ret:
            ret = ''

        return ret

    def issue_amount(self):
        ret = ''
        for n, text in enumerate(self.textlist):
            if re.search('本期发行金额|本期.*规模', text) or re.search('发行金额', text):
                if len(text) > 40:
                    ret = text
                    break
                else:
                    ret = ''.join(self.textlist[n:(n+5)])
                    break
        ret = re.findall('本期.*?元|本期.*?亿|发行金额.*?元|发行金额.*?亿', ret)
        if ret:
            ret = ret[0]
        else:
            ret = ''

        ret = re.sub('[：,，:“”【】 为]', '', ret)
        if re.search('[0-9]', ret):
            if '亿' in ret:
                ret = re.sub(u'[\u4e00-\u9fff]', '', ret)
                ret = str(float(ret) * 100000000)
        else:
            ret = re.findall('人民币.*元', ret)
            if ret:
                ret = ret[0]
                ret = re.sub('人民币', '', ret)
                ret = re.sub('元', '', ret)
            else:
                ret = ''
        if not re.search('[0-9]', ret):
            ret = str(float(chinese_to_arabic(ret)))
        return ret

    def issue_period(self):
        ret = ''
        for n, text in enumerate(self.textlist):
            if re.search('期限|发行期限', text):
                if re.search('还本付息', text):
                    continue
                ret = ''.join(self.textlist[n:(n+4)])
                break
        ret = re.findall('期限.*?[天年N]', ret)
        if ret:
            ret = re.sub('期限|：', '', ret[0])
            if ret.endswith('N'):
                ret += '年'
        else:
             ret = ''
        ret = re.sub('[：,，:“”【】 为]', '', ret)
        return ret

    def face_value(self):
        return '100'

    def issue_date(self):
        ret = ''
        for n, text in enumerate(self.textlist):
            if re.search('发行日：|发行首日：|发行日期：', text):
                if re.search('还本付息', text):
                    continue
                if (len(text) > 30) & (('12' not in text) or ('2018' not in text) or ('月' not in text) or ('日' not in text)):
                    continue
                if re.search('披露|安排|说明|规定|基本', text):
                    continue
                t = ''.join(self.textlist[(n):(n+2)])
                t = re.sub('缴款日', '', t)

                s = re.search('日.*?日', t)

                if s:
                    start, end = s.span()
                    ret = t[(start+1):end]
                    if (t[end] == '-') or (t[end] == '至') or (t[end] == '、'):
                        a = re.findall('.*?日', t[(end+1):])
                        if not a:
                            ret = re.sub('：|【|】|期|指|〔|〕| |为|\[|\]|\(|\)|（|）', '', ret)
                            return ret
                        ret = ret+'至'+a[0]
                    ret = re.sub('：|【|】|期|指|〔|〕| |为|\[|\]|\(|\)|（|）', '', ret)
                    return ret
                else:
                    print('warning:', t)

                return ret

    def value_date(self):

        ret = ''
        for n, text in enumerate(self.textlist):
            if re.search('起息日', text):
                if re.search('还本付息', text):
                    continue
                if (len(text) > 30) & (('12' not in text) or ('2018' not in text) or ('月' not in text) or ('日' not in text)):
                    continue
                if re.search('披露|安排|说明|规定|基本', text):
                    continue
                t = ''.join(self.textlist[(n):(n+12)])
                t = re.sub('缴款日|到.*日', '', t)
                s = re.search('日.*?日', t)

                if s:
                    start, end = s.span()
                    ret = t[(start+1):end]
                    if (t[end] == '-') or (t[end] == '至') or (t[end] == '、'):
                        a = re.findall('.*?日', t[(end+1):])
                        if not a:
                            ret = re.sub('：|【|】|期|指|〔|〕| |为|\[|\]|\(|\)|（|）', '', ret)
                            return ret
                        ret = ret+'至'+a[0]
                    ret = re.sub('：|【|】|期|指|〔|〕| |为|\[|\]|\(|\)|（|）', '', ret)
                    return ret
                else:
                    print('warning:', t)

                return ret


if __name__ == '__main__':

    # single instance:

    folder = 'newpoc/txts'
    filelist = os.listdir(folder)
    filelist = [file for file in filelist if re.search('.txt', file)]

    # file = filelist[0]
    # p = poc(folder, file)

    data = []


    for n, file in enumerate(filelist[:]):
        try:
            print(n)
            p = poc(folder, file)
            data.append(p.data)
        except Exception as err:
            print(err)
            print(file)

    with open('result.json', 'w', encoding='utf-8', ) as fp:
        json.dump(data, fp, indent=4, ensure_ascii=False)