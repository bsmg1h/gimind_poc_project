import os
import re
import shutil
from bs4 import BeautifulSoup
import json

import sys
import importlib
importlib.reload(sys)


def is_number(s):
    if not s:
        return False

    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False

def locate(pattern, textlist):
    pos = 0
    for text in textlist:
        if re.search(pattern, text):
            break
        pos += 1
    return pos


def locatesearchlist(patternlist, textlist):
    for pos in range(len(textlist) - len(patternlist)):
        tl = textlist[pos:(pos+len(patternlist))]
        for i in range(len(patternlist)):

            if patternlist[i] == '':
                if tl[i] == '':
                    pass
                else:
                    break
            else:
                if re.search(patternlist[i], tl[i]):
                    pass
                else:
                    break

            if i == len(patternlist) - 1:
                return pos, pos+len(patternlist), tl
    return -1, -1, []

def locatematchlist(patternlist, textlist):
    for pos in range(len(textlist) - len(patternlist)):
        tl = textlist[pos:(pos+len(patternlist))]
        for i in range(len(patternlist)):

            if patternlist[i] == '':
                if tl[i] == '':
                    pass
                else:
                    break
            else:
                if re.match(patternlist[i], tl[i]):
                    pass
                else:
                    break

            if i == len(patternlist) - 1:
                return pos, pos+len(patternlist), tl
    return -1, -1, []

def locateequallist(patternlist, textlist):
    for pos in range(len(textlist) - len(patternlist)):
        tl = textlist[pos:(pos+len(patternlist))]
        for i in range(len(patternlist)):

            if patternlist[i] == tl[i]:
                pass
            else:
                break

            if i == len(patternlist) - 1:
                return pos, pos+len(patternlist), tl
    return -1, -1, []

class PeriodicReport:

    def __init__(self, reportsoup):

        self.soup = reportsoup
        self.textlist = reportsoup.get_text().split(' ')

    def companyProfile(self):

        dataname = '公司概况'
        formstart = locatesearchlist(['公司股票简况'], self.textlist)[0] + 1
        formcontinue = locatesearchlist(['[A-Z]股'], self.textlist[formstart:])[0]

        formdata = self.textlist[formstart: formstart + 2 * formcontinue]

        formulateddata = {}

        columnkeys = self.textlist[formstart: formstart + formcontinue]

        for i in range(len(columnkeys)):
            formulateddata[columnkeys[i]] = formdata[i + len(columnkeys)]

        return {
            '数据名称': dataname,
            '格式化数据': formulateddata
        }


    def financialData(self):

        dataname = self.textlist[locatesearchlist(['币种：人民币'], self.textlist)[0]-2]
        unit = self.textlist[locatesearchlist(['币种：人民币'], self.textlist)[0]-1].split('：')[-1]
        currency = self.textlist[locatesearchlist(['币种：人民币'], self.textlist)[0]].split('：')[-1]

        formstart = locatesearchlist(['币种：人民币'], self.textlist)[0] + 1
        formcontinue = locatematchlist(['', '[0-9]', ''], self.textlist[formstart:])[0]
        formcontinue1 = locatesearchlist(['前十'], self.textlist[formstart:])[0]
        if formcontinue1 != -1:
            formcontinue = min(formcontinue, formcontinue1)

        formdata = self.textlist[formstart: formstart+formcontinue]
        formulateddata = {}

        #  1. 单元格内分行处理
        patternexist = locatematchlist(['（|增减'], formdata)
        while patternexist[2]:
            temptext = formdata.pop(patternexist[0])
            formdata[patternexist[0]-1] += temptext
            patternexist = locatematchlist(['（'], formdata)

        #  2. 合并单元格（列）处理
        patternexist = locatematchlist(['调整后', '调整前'], formdata)
        while patternexist[2]:
            del formdata[patternexist[0]:patternexist[1]]
            temppos = locatematchlist(['上年'], formdata)[0]
            temptext = formdata[temppos]
            formdata.insert(temppos+1, ' ' + temptext+'调整前')
            formdata[temppos] = ' ' + temptext+'调整后'
            patternexist = locatematchlist(['调整后', '调整前'], formdata)

        #  3. 分页符处理
        patternexist = locatematchlist(['\n', '', '[0-9]', '/', '[0-9]', ''], formdata)
        while patternexist[2]:
            del formdata[patternexist[0]: patternexist[1]]
            patternexist = locatematchlist(['\n', '', '[0-9]', '/', '[0-9]', ''], formdata)

        patternexist = locatematchlist(['\n2', ''], formdata)
        while patternexist[2]:
            del formdata[patternexist[0]: patternexist[1]]
            patternexist = locatematchlist(['\n2', ''], formdata)


        ncolumnkeys = locate('总资产', formdata) - 1
        columnkeys = formdata[1:(ncolumnkeys+1)]

        formdata = formdata[(ncolumnkeys+1):]

        while len(formdata) > ncolumnkeys:

            if formdata[0] == '' or formdata[0] == '\n':
                columnkeys = formdata[1:(ncolumnkeys + 1)]
                formdata = formdata[(ncolumnkeys + 1):]
                continue

            while re.match('[\u4e00-\u9fff]', formdata[1]):
                temptext = formdata.pop(1)
                formdata[0] += temptext

            formulateddata[formdata[0]] = {}

            for i in range(len(columnkeys)):
                formulateddata[formdata[0]][columnkeys[i]] = formdata[i+1]

            formdata = formdata[(ncolumnkeys + 1):]




        return {
            '数据名称': dataname.split('的')[-1],
            '单位': unit,
            '币种': '人民币',
            '数据': formdata,
            '格式化数据': formulateddata
        }


if __name__ == '__main__':

    folder = 'dingqibaogaoHTML'
    zyfilelist = os.listdir(folder)
    zyfilelist = [file for file in zyfilelist if re.search('zy.pdf.html', file)]

    for file in zyfilelist:
        try:
            soup = BeautifulSoup(open(folder + '/' + file, 'r', encoding='utf-8'))
            output = {}
            output['公司概况'] = PeriodicReport(soup).companyProfile()
            output['财务数据'] = PeriodicReport(soup).financialData()

            for k0, v0 in output['财务数据']['格式化数据'].items():
                for k1, v1 in v0.items():
                    if "百分点" in v1:
                        print(file)
                        print(output['财务数据']['格式化数据'][k0])

                        percentagechange = re.findall("[0-9].*[0-9]", output['财务数据']['格式化数据'][k0][k1])[0]
                        percentagechange = float(percentagechange)
                        if output['财务数据']['格式化数据'][k0][k1].startswith('减少'):
                            percentagechange *= -1
                        if '上年同期' in output['财务数据']['格式化数据'][k0]:
                            if is_number(output['财务数据']['格式化数据'][k0]['上年同期']):
                                percentagelast = float(output['财务数据']['格式化数据'][k0]['上年同期'])
                                percentagechangemodify = str(percentagechange / percentagelast * 100)
                                percentagechangemodify = percentagechangemodify[:percentagechangemodify.index(".")+3]
                                output['财务数据']['格式化数据'][k0][k1] = percentagechangemodify
                        if ' 上年同期调整后' in output['财务数据']['格式化数据'][k0]:
                            if is_number(output['财务数据']['格式化数据'][k0][' 上年同期调整后']):
                                percentagelast = float(output['财务数据']['格式化数据'][k0][' 上年同期调整后'])
                                percentagechangemodify = str(percentagechange / percentagelast * 100)
                                percentagechangemodify = percentagechangemodify[:percentagechangemodify.index(".") + 3]
                                output['财务数据']['格式化数据'][k0][k1] = percentagechangemodify
                        if ' 上年度末调整后' in output['财务数据']['格式化数据'][k0]:
                            if is_number(output['财务数据']['格式化数据'][k0][' 上年度末调整后']):
                                percentagelast = float(output['财务数据']['格式化数据'][k0][' 上年度末调整后'])
                                percentagechangemodify = str(percentagechange / percentagelast * 100)
                                percentagechangemodify = percentagechangemodify[:percentagechangemodify.index(".") + 3]
                                output['财务数据']['格式化数据'][k0][k1] = percentagechangemodify

                        print(output['财务数据']['格式化数据'][k0])


            json.dump(output, open('dingqibaogaoJSON/' + file + '.json', 'w', encoding='utf-8'),
                      ensure_ascii=False,
                      indent=4)
        except Exception as err:
            print(err)
            print(file)
            pass

    soup = BeautifulSoup(open(folder + '/' + '601933_2017_zzy.pdf.html', 'r', encoding='utf-8'))
    soup.get_text().split(' ')






