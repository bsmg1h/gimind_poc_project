import os
import sys
import re
import importlib
importlib.reload(sys)

from pdfminer.pdfparser import PDFParser,PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LTTextBoxHorizontal,LAParams
from pdfminer.pdfinterp import PDFTextExtractionNotAllowed

from urllib.request import urlopen

def parse(path):
    fp = open(path, 'rb') # 以二进制读模式打开
    # fp = urlopen(path)      # 远程文件

    #用文件对象来创建一个pdf文档分析器
    praser = PDFParser(fp)
    # 创建一个PDF文档
    doc = PDFDocument()
    # 连接分析器 与文档对象
    praser.set_document(doc)
    doc.set_parser(praser)

    # 提供初始化密码
    # 如果没有密码 就创建一个空的字符串
    doc.initialize()

    # 检测文档是否提供txt转换，不提供就忽略
    if not doc.is_extractable:
        raise PDFTextExtractionNotAllowed
    else:
        # 创建PDf 资源管理器 来管理共享资源
        rsrcmgr = PDFResourceManager()
        # 创建一个PDF设备对象
        laparams = LAParams()

        device = PDFPageAggregator(rsrcmgr, laparams=laparams)
        # 创建一个PDF解释器对象
        interpreter = PDFPageInterpreter(rsrcmgr, device)

        # 循环遍历列表，每次处理一个page的内容
        ret = []
        i = 0

        for page in doc.get_pages(): # doc.get_pages() 获取page列表
            i += 1
            if i > 10:
                break
            interpreter.process_page(page)
            # 接受该页面的LTPage对象
            layout = device.get_result()
            # 这里layout是一个LTPage对象 里面存放着 这个page解析出的各种对象 一般包括LTTextBox, LTFigure, LTImage, LTTextBoxHorizontal 等等 想要获取文本就获得对象的text属性，
            for out in layout:
                try:
                    results = out.get_text()
                    print(results)
                    ret.append(results)
                except Exception as err:
                    pass
        return ret

if __name__ == '__main__':

    # single instance:

    folder = 'poc/pdfs'
    filelist = os.listdir(folder)
    filelist = [file for file in filelist]

    file = filelist[0]
    p = parse(folder + '/' + file)


    # for file in filelist:
    #     try:
    #         p = poc(folder, file)
    #     except Exception as err:
    #         print(err)
    #         print(file)