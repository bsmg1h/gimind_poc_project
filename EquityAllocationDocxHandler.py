# import docx
import os
import re
try:
    from xml.etree.cElementTree import XML
except ImportError:
    from xml.etree.ElementTree import XML
import zipfile
import json

"""
Module that extract text from MS XML Word document (.docx).
(Inspired by python-docx <https://github.com/mikemaccana/python-docx>)
"""

WORD_NAMESPACE = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'

PARA = WORD_NAMESPACE + 'p'
TEXT = WORD_NAMESPACE + 't'

def get_docx_text(path):
    """
    Take the path of a docx file as argument, return the text in unicode.
    """
    document = zipfile.ZipFile(path)
    xml_content = document.read('word/document.xml')
    document.close()
    tree = XML(xml_content)

    paragraphs = []
    for paragraph in tree.getiterator(PARA):
        texts = [node.text
                 for node in paragraph.getiterator(TEXT)
                 if node.text]
        if texts:
            paragraphs.append(''.join(texts))

    return paragraphs



def EquityAllocation(text):
    result = {}

    def locate(pattern, textlist):
        pos = 0
        for para in textlist:
            if re.search(pattern, para):
                break
            pos += 1
        return pos

    end = locate("分红送转", text)
    text = text[:end+1]

    def Header(text):
        KEYSpos = [0,2,4]
        thistext = text[0]
        thistext = [t for t in re.split(r" |:|：|\t", thistext) if t]
        for pos in KEYSpos:
            result[thistext[pos]] = thistext[pos + 1]
        return thistext

    def AnnouncementName(text):
        result['公告名称'] = text[1]

    def AnnouncementClass(text):
        result['公告分类'] = '权益分派实施公告'


    def ImportantNotes(text):
        ImportantNotesStart = locate("每股分配比例", text)
        RelatedDates = locate('相关日期', text)
        DifferentiationDividends = locate('分红送转', text)

        result['每股分配比例'] = {}
        result['相关日期'] = {}
        result['差异化分红送转'] = {}

        for i in range(ImportantNotesStart+1, RelatedDates):
            spliti = re.search('[0-9]', text[i])
            if spliti:
                spliti = spliti.start()
            result['每股分配比例'][text[i][:spliti]] = text[i][spliti:]

        Ncolumns = int((DifferentiationDividends - (RelatedDates + 1))/2)
        for i in range(RelatedDates+1, RelatedDates+1+Ncolumns):
            result['相关日期'][text[i]] = text[i + Ncolumns]

        result['差异化分红送转'] = text[DifferentiationDividends][-1]

        return ImportantNotesStart, RelatedDates, DifferentiationDividends

    Header(text)
    AnnouncementName(text)
    AnnouncementClass(text)
    ImportantNotes(text)

    return result

def main():


    filename = "权益分派"
    files = os.listdir(filename)
    files = [file for file in files if not file.startswith('~')]

    for file in files:
        text = get_docx_text(filename + '/' + file)
        result = EquityAllocation(text)
        print(result)

        json.dump(obj=result,
                  fp=open('权益分派信息抽取' + '/' + file + '.json', 'w', encoding='utf-8'),
                  indent=4,
                  ensure_ascii=False)



if __name__ == '__main__':

    main()